<?php


function iqbi_api_request_token_servlet(){
  global $base_url;
  global $user;

  $run_close_js = FALSE;
  $content = '';

  // cleans out the token variable if comming from
  // connect to QuickBooks button
  if ( isset($_GET['start'] ) ) {
    unset($_SESSION['token']);
  }

  $oauth_consumer_key = variable_get('iqbi_oauth_consumer_key', NULL);
  $oauth_consumer_secret = variable_get('iqbi_oauth_consumer_secret', NULL);

  try {
    $oauth = new OAuth( $oauth_consumer_key, $oauth_consumer_secret, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
    $oauth->enableDebug();
    $oauth->disableSSLChecks(); //To avoid the error: (Peer certificate cannot be authenticated with given CA certificates)
    if (!isset( $_GET['oauth_token'] ) && !isset($_SESSION['token']) ){
      // step 1: get request token from Intuit
      $request_token = $oauth->getRequestToken( IQBI_OAUTH_REQUEST_URL, $base_url . '/intuit-quickbooks-integration/request-token-servlet' );
      $_SESSION['secret'] = $request_token['oauth_token_secret'];
      // step 2: send user to intuit to authorize
      header('Location: '. IQBI_OAUTH_AUTHORISE_URL .'?oauth_token='.$request_token['oauth_token']);
    }

    if ( isset($_GET['oauth_token']) && isset($_GET['oauth_verifier']) ){
      // step 3: request a access token from Intuit
      $oauth->setToken($_GET['oauth_token'], $_SESSION['secret']);
      $access_token = $oauth->getAccessToken( IQBI_OAUTH_ACCESS_URL );

      $_SESSION['token'] = serialize( $access_token );
      $_SESSION['realmId'] = $_REQUEST['realmId'];  // realmId is legacy for customerId
      $_SESSION['dataSource'] = $_REQUEST['dataSource'];

      $token = $_SESSION['token'] ;
      $realmId = $_SESSION['realmId'];
      $dataSource = $_SESSION['dataSource'];
      $secret = $_SESSION['secret'] ;

      $run_close_js = TRUE;
    }

    if(isset($_SESSION['token'])){
      // Mark what is required
      $required = array(
        'company_id',
        'consumer_key',
        'consumer_secret',
        'access_token',
        'access_secret',
      );

      if(!($iqbi_api_client_credentials = variable_get("iqbi_api_uid_{$user->uid}", ''))){
        $token = unserialize( $_SESSION['token'] );
        $tmpCredentials = array(
          'access_token' => $token['oauth_token'],
          'access_secret' => $token['oauth_token_secret'],
          'company_id' => $_SESSION['realmId'],
          'consumer_key' => $oauth_consumer_key,
          'consumer_secret' => $oauth_consumer_secret
        );

        variable_set("iqbi_api_uid_{$user->uid}", $tmpCredentials);

        rules_invoke_event('iqbi_authenticate_success');
      }

      $run_close_js = TRUE;
    }

    if($run_close_js){
      drupal_add_js('window.opener.location.href = window.opener.location.href;
              window.close();', array('type' => 'inline'));
    }

  } catch(OAuthException $e) {
    $content .= '<p>Got auth exception</p>';
    $content .= '<code>' . print_r($e, TRUE) . '</code>';
  }

  $content = '<div><h1>Connecting to Quickbooks</h1></div>' . $content;

  return $content;
}

function iqbi_api_disconnect() {
  global $user;

  // Load the library
  libraries_load('qbo');

  // Mark what is required
  $required = array(
    'company_id',
    'consumer_key',
    'consumer_secret',
    'access_token',
    'access_secret',
  );

  if(($iqbi_api_client_credentials = variable_get("iqbi_api_uid_{$user->uid}", ''))){
    // Iterate the required items and fetch the data
    foreach ($required as $key) {
      // If we cannot get a value, then we cannot get a client
      if(!isset($iqbi_api_client_credentials[$key]) || empty($iqbi_api_client_credentials[$key]) || !(${$key} = $iqbi_api_client_credentials[$key]) ){
        return NULL;
      }
    }
  }else{
    return NULL;
  }

  // Authenticate via OAuth and create a Data Service object
  $request_validator = new OAuthRequestValidator($access_token, $access_secret, $consumer_key, $consumer_secret);
  $service_type = IntuitServicesType::QBO;
  $service_context = new ServiceContext($company_id, $service_type, $request_validator);
  $service_context->IppConfiguration->Logger->RequestLog->EnableRequestResponseLogging = FALSE;


  // Prep Platform Services
  $patform_service = new PlatformService($service_context);

  // Get App Menu HTML
  $Respxml = $patform_service->Disconnect();


  if (empty($Respxml) || $Respxml->ErrorCode == '0'){
    unset($_SESSION['token']);
    unset($_SESSION['dataSource']);
    unset($_SESSION['realmId']);
    unset($_SESSION['secret']);
    variable_del("iqbi_api_uid_{$user->uid}");
    return TRUE;
  }else{
    // force disconnect because oauth is invalid anyway
    unset($_SESSION['token']);
    unset($_SESSION['dataSource']);
    unset($_SESSION['realmId']);
    unset($_SESSION['secret']);
    variable_del("iqbi_api_uid_{$user->uid}");

    if ($Respxml->ErrorCode  == '270'){
      drupal_set_message(t('OAuth Token Rejected!'), 'error');
    }
    watchdog('intuit_quickbooks_integration', t('Disconnect failed. Error code: @error_code', array('@error_code' => $Respxml->ErrorCode)));
    return FALSE;
  }
}

/**
 * Create a QBO API client class.
 *
 * @return
 *   A Quickbooks Online API Client class if all connection settings are
 *   present, otherwise NULL.
 */
function iqbi_api_client() {
  global $user;

  // Load the library
  libraries_load('qbo');

  // Mark what is required
  $required = array(
    'company_id',
    'consumer_key',
    'consumer_secret',
    'access_token',
    'access_secret',
  );

  if(($iqbi_api_client_credentials = variable_get("iqbi_api_uid_{$user->uid}", ''))){
    // Iterate the required items and fetch the data
    foreach ($required as $key) {
      // If we cannot get a value, then we cannot get a client
      if(!isset($iqbi_api_client_credentials[$key]) || empty($iqbi_api_client_credentials[$key]) || !(${$key} = $iqbi_api_client_credentials[$key]) ){
        return NULL;
      }
    }
  }else{
    return NULL;
  }

  // Authenticate via OAuth and create a Data Service object
  $request_validator = new OAuthRequestValidator($access_token, $access_secret, $consumer_key, $consumer_secret);
  $service_type = IntuitServicesType::QBO;
  $service_context = new ServiceContext($company_id, $service_type, $request_validator);
  $service_context->IppConfiguration->Logger->RequestLog->EnableRequestResponseLogging = FALSE;
  $data_service = new DataService($service_context);

  return $data_service;
}

/**
 * Perform a query against the Quickbooks API.
 *
 * Responses will be cached both statically and in the database.
 *
 * @param $query
 *   The SQL query to perform.
 * @param $reset
 *   TRUE if the cache should be bypassed. Defaults to FALSE.
 * @return
 *   The query response, or NULL if nothing was returned or the call
 *   failed.
 */
function iqbi_api_query($query, $reset = FALSE) {
  $responses = &drupal_static(__FUNCTION__, array());

  // Load the library
  libraries_load('qbo');

  // Create a key for this query
  $key = 'qbo:query:' . md5(strtolower($query));

  // Check the static cache
  if (!isset($responses[$key]) || $reset) {
    // Initialize the static cache
    $responses[$key] = array();

    // Check the database cache
    if (!$reset && ($cache = cache_get($key))) {
      // Extract the cached data
      $responses[$key] = $cache->data;
    }
    else {
      // Get a client
      if ($client = iqbi_api_client()) {
        // Perform the query, using pages if the result sets are larger
        // than the defined page size.
        $page = 0;
        while (TRUE) {
          // Fetch the results
          $response = $client->Query($query, $page, IQBI_API_QUERY_PAGE_SIZE);

          // Increment the page for the next potential run
          $page++;

          // See if results were returned
          if (is_array($response)) {
            // Add the response to the overall results, if something was
            // returned.
            $responses[$key] = array_merge($responses[$key], $response);

            // If the size of the response is the size of the page, we
            // need to move on to the next page
            if (count($response) == IQBI_API_QUERY_PAGE_SIZE) {
              continue;
            }
          }

          // Stop the looping
          break;
        }

        // Cache the data for next time, if data was returned
        if ($responses[$key]) {
          cache_set($key, $responses[$key], 'cache', CACHE_TEMPORARY);
        }
      }
    }
  }

  return $responses[$key];
}

function iqbi_api_add($ipp_object){
  if ($client = iqbi_api_client()) {
    try{
      $result_ipp_obj = $client->Add($ipp_object);
      return $result_ipp_obj;
    }catch(Exception $e){
      watchdog('intuit_quickbooks_integration', t("Exception occured:\r\n@exception", array('@exception' => (string) $e)));
      return;
    }
    return;
  }
}

function iqbi_api_update($ipp_object){
  if ($client = iqbi_api_client()) {
    try{
      $result_ipp_obj = $client->Update($ipp_object);
      return $result_ipp_obj;
    }catch(Exception $e){
      watchdog('intuit_quickbooks_integration', t("Exception occured:\r\n@exception", array('@exception' => (string) $e)));
      return;
    }
    return;
  }
}

function iqbi_api_deactivate($ipp_object){
  if ($client = iqbi_api_client()) {
    try{
      // some entities like Employee doesn't work with Delete() so we have to update the Entity's Active field to false.
      $ipp_object->Active = 'false';
      $result_ipp_obj = $client->Update($ipp_object);
      return $result_ipp_obj;
    }catch(Exception $e){
      watchdog('intuit_quickbooks_integration', t("Exception occured:\r\n@exception", array('@exception' => (string) $e)));
      return;
    }
    return;
  }
}

function iqbi_api_delete($ipp_object){
  if ($client = iqbi_api_client()) {
    try{
      $result_ipp_obj = $client->Delete($ipp_object);
      return $result_ipp_obj;
    }catch(Exception $e){
      watchdog('intuit_quickbooks_integration', t("Exception occured:\r\n@exception", array('@exception' => (string) $e)));
      return;
    }
    return;
  }
}
