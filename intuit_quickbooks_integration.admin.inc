<?php

function iqbi_config_form($form, &$form_state){
  $form = array();

  $form['message'] = array(
    '#markup' => t(
      'Please download the PHP SDK from !link and put it inside your libraries folder (libraries/qbo). Change the base url in sdk.config file (libraries/qbo/sdk.config) to switch over to sandbox and production.',
      array(
        '!link' => l('QuickBooks PHP SDK Download Page', 'https://developer.intuit.com/docs/0100_accounting/0500_developer_kits/0210_ipp_php_sdk_for_quickbooks_v3')
      )
    )
  );

  $form['iqbi_oauth_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Oauth consumer key'),
    '#default_value' => variable_get('iqbi_oauth_consumer_key', NULL)
  );

  $form['iqbi_oauth_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Oauth consumer secret'),
    '#default_value' => variable_get('iqbi_oauth_consumer_secret', NULL)
  );

  return system_settings_form($form);
}
